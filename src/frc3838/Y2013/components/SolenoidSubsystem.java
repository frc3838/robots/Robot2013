package frc3838.Y2013.components;


import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc3838.Y2013.utils.LOG;



public abstract class SolenoidSubsystem extends Subsystem
{
    private int solenoidEngageChannelAddr;
    private int solenoidDisengageChannelAddr;

    protected Solenoid engageSolenoid;
    protected Solenoid disengageSolenoid;


    public abstract boolean isEnabled();


    /**
     * This is a protected constructor to ensure a single instance (i.e. singleton pattern). Use the static {@link #getInstance()} method to obtain a reference to the subsystem. For
     * example, instead of doing this:
     * <pre>
     *     FiringPinSubsystem subsystem = new FiringPinSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     FiringPinSubsystem subsystem = FiringPinSubsystem.getInstance();
     * </pre>
     */
    protected SolenoidSubsystem(int solenoidEngageChannelAddr, int solenoidDisengageChannelAddr)
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            this.solenoidEngageChannelAddr = solenoidEngageChannelAddr;
            this.solenoidDisengageChannelAddr = solenoidDisengageChannelAddr;
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in FiringPinSubsystem() constructor", e);
        }
    }


    protected void init()
    {
        if (isEnabled())
        {
            try
            {
                LOG.debug("Initializing " + getName());
                engageSolenoid = new Solenoid(solenoidEngageChannelAddr);
                disengageSolenoid = new Solenoid(solenoidDisengageChannelAddr);
                LOG.debug(getName() + " initialization completed successfully");
                disengage();

            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in " + getName() + ".init()", e);
            }
        }
        else
        {
            LOG.info(getName() + " is disabled and will not be initialized");
        }
    }


    protected void engage()
    {
        try
        {
            if (isEnabled() && engageSolenoid != null && disengageSolenoid != null)
            {
                disengageSolenoid.set(false);
                engageSolenoid.set(true);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in " + getName() + " .engage()", e);
        }
    }


    protected void disengage()
    {
        try
        {
            if (isEnabled() && engageSolenoid != null && disengageSolenoid != null)
            {
                engageSolenoid.set(false);
                disengageSolenoid.set(true);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in " + getName() + " .disengage()", e);
        }

    }


    public void initDefaultCommand()
    {

    }
}
