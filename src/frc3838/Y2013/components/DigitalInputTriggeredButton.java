package frc3838.Y2013.components;


import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.buttons.Button;



public class DigitalInputTriggeredButton extends Button
{
    DigitalInput digitalInput;


    public DigitalInputTriggeredButton(DigitalInput digitalInput)
    {
        this.digitalInput = digitalInput;
    }


    public DigitalInputTriggeredButton(int digitalInoutChannel)
    {
        digitalInput = new DigitalInput(digitalInoutChannel);
    }


    public DigitalInput getDigitalInput()
    {
        return digitalInput;
    }


    public boolean get()
    {
        return digitalInput.get();
    }
}
