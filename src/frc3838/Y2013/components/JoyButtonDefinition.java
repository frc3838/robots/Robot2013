package frc3838.Y2013.components;


import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;



public class JoyButtonDefinition
{
    public final Joystick joystick;
    
    public final int buttonNumber;
    
    public final JoystickButton joystickButton;


    public JoyButtonDefinition(Joystick joystick, int buttonNumber)
    {
        this.joystick = joystick;
        this.buttonNumber = buttonNumber;
        joystickButton = new JoystickButton(joystick, buttonNumber);
    }


    public Joystick getJoystick()
    {
        return joystick;
    }


    public int getButtonNumber()
    {
    
        return buttonNumber;
    }


    public JoystickButton getJoystickButton()
    {
        return joystickButton;
    }
}
