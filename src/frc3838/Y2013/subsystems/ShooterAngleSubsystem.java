package frc3838.Y2013.subsystems;


import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.components.SolenoidSubsystem;
import frc3838.Y2013.utils.LOG;



public class ShooterAngleSubsystem extends SolenoidSubsystem
{
    public static boolean isEnabled = true;

    private static final ShooterAngleSubsystem singleton = new ShooterAngleSubsystem();


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such as <tt>new ShooterAngleSubsystem()</tt> in order to use ensure only a
     * single instance of a Subsystem is created. This is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     FiringPinSubsystem subsystem = new FiringPinSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     FiringPinSubsystem subsystem = FiringPinSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static ShooterAngleSubsystem getInstance()
    {return singleton;}


    public void setToHighShot()
    {
        LOG.debug("setToHighShot() called");
        engage();
    }


    public void setToLowShot()
    {
        LOG.debug("setToLowShot() called");
        disengage();
    }


    private ShooterAngleSubsystem()
    {
        super(RobotMap2013.Solenoids.SHOOTER_ANGLE_DISENGAGE_ADDR, RobotMap2013.Solenoids.SHOOTER_ANGLE_ENGAGE_ADDR);
    }


    public boolean isEnabled()
    {
        return isEnabled;
    }
}
