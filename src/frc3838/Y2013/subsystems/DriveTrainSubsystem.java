package frc3838.Y2013.subsystems;


import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.commands.drive.DriveCommand;
import frc3838.Y2013.utils.LOG;



/** @noinspection PointlessBooleanExpression, ConstantConditions , BooleanMethodNameMustStartWithQuestion */
public class DriveTrainSubsystem extends Subsystem
{

    public static final boolean isEnabled = true;

    private static final double deadZoneThreshold = 0.05;

    /** @noinspection PointlessBooleanExpression */
    public static final boolean isClimbSystemEnabled = isEnabled && true;

    private static final boolean invertLeftSide = false;
    private static final boolean invertRightSide = false;

    private static boolean useTankDrive = false;
    private static boolean useTankDriveDriverSelection = false;

    private static boolean useClimbSplitSticks = true;
    private static boolean isUseClimbSplitSticksOpsSelection = false;

    private static boolean inClimbMode = false;
    private static boolean decreaseSensitivityAtLowerSpeeds = false;

    private static DigitalInput leftClawLimitSwitch;
    private static DigitalInput rightClawLimitSwitch;

    private static final DriveTrainSubsystem singleton = new DriveTrainSubsystem();

    private static RobotDrive robotDrive;
    private static Solenoid climbEngageSolenoid;
    private static Solenoid climbDisengageSolenoid;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such as <tt>new DriveTrainSubsystem()</tt> in order to use ensure only a
     * single instance of a Subsystem is created. This is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static DriveTrainSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern). Use the static {@link #getInstance()} method to obtain a reference to the subsystem. For
     * example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     */
    private DriveTrainSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();

        }
        catch (Exception e)
        {
            LOG.error("An exception occurred during initialization in DriveTrainSubsystem() constructor", e);
        }
    }


    private void init()
    {
        if (isEnabled)
        {
            try
            {
                LOG.debug("Initializing DriveTrainSubsystem");
                initRobotDrive();
                initClimbSystem();
                updateSensitivityModeDisplay();
                LOG.debug("DriveTrainSubsystem initialization completed successfully");

            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in DriveTrainSubsystem.init()", e);
            }

        }
        else
        {
            LOG.info("DriveTrainSubsystem is disabled and will not be initialized");
        }
    }


    /**
     * Toggles the flag that indicates if the drive should be set to decrease the joystick sensitivity at lower speeds. When this flag is true, it decreases the sensitivity at
     * lower speeds
     *
     * @return the new value of the <tt>decreaseSensitivityAtLowerSpeeds</tt> flag
     */
    public boolean toggleDecreaseSensitivityAtLowerSpeeds()
    {
        decreaseSensitivityAtLowerSpeeds = !decreaseSensitivityAtLowerSpeeds;
        updateSensitivityModeDisplay();
        return decreaseSensitivityAtLowerSpeeds;
    }


    /**
     * Sets the flag that indicates if the drive should be set to decrease the joystick sensitivity at lower speeds. Setting this flag to true decreases the sensitivity at lower
     * speeds.
     *
     * @param value the new flag value
     */
    public void setDecreaseSensitivityAtLowerSpeeds(boolean value)
    {
        DriveTrainSubsystem.decreaseSensitivityAtLowerSpeeds = value;
        updateSensitivityModeDisplay();
    }


    private String sensitivityModeStatus()
    {
        return decreaseSensitivityAtLowerSpeeds ? "Low Sens" : "Standard";
    }


    public void climbViaSingleStick(GenericHID stick)
    {
        try
        {
            if (isEnabled && robotDrive != null)
            {
                double Y = stick.getY();
                if (Math.abs(Y) < deadZoneThreshold) {Y = 0;}

                robotDrive.arcadeDrive(Y, 0, decreaseSensitivityAtLowerSpeeds);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.climbViaSingleStick()", e);
        }

    }


    /**
     * Provide tank steering using the stored robot configuration. drive the robot using two joystick inputs. The Y-axis will be selected from each Joystick object.
     *
     * @param leftStick  The joystick to control the left side of the robot.
     * @param rightStick The joystick to control the right side of the robot.
     */
    public void driveViaTankDrive(GenericHID leftStick, GenericHID rightStick)
    {
        try
        {
            if (isEnabled && robotDrive != null)
            {
                double leftY = leftStick.getY();
                double rightY = rightStick.getY();
                if (Math.abs(leftY) < deadZoneThreshold) {leftY = 0;}
                if (Math.abs(rightY) < deadZoneThreshold) {rightY = 0;}

                if (inClimbMode)
                {
                    if (RobotMap2013.IN_DEBUG_MODE)
                    {
                        SmartDashboard.putBoolean("Left Claw Sw", leftClawLimitSwitch.get());
                        SmartDashboard.putBoolean("Right Claw Sw", rightClawLimitSwitch.get());
                    }


                    //true is switch not "triggered" (i.e. pushed)
                    //neg is going up
                    if (!leftClawLimitSwitch.get() && (leftY < 0)) {leftY = 0;}
                    if (!rightClawLimitSwitch.get() && (rightY < 0)) {rightY = 0;}
                }

                robotDrive.tankDrive(leftY, rightY, decreaseSensitivityAtLowerSpeeds);

            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.driveViaTankDrive()", e);
        }
    }


    /**
     * Drives the DriveTrainSubsystem using arcade drive.
     *
     * @param stick The joystick to use for Arcade single-stick driving. The Y-axis will be selected for forwards/backwards and the X-axis will be selected for rotation rate.
     *
     * @see RobotDrive#arcadeDrive(GenericHID)
     */
    public void driveViaArcadeDrive(GenericHID stick)
    {
        try
        {
            if (isEnabled && robotDrive != null)
            {
                //arcadeDrive(stick.getY(), stick.getX(), squaredInputs);

                double x = stick.getX();
                double y = stick.getY();

                if (Math.abs(x) < deadZoneThreshold)
                {
                    x = 0;
                }

                if (Math.abs(y) < deadZoneThreshold)
                {
                    y = 0;
                }

                robotDrive.arcadeDrive(y, x, decreaseSensitivityAtLowerSpeeds);
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.driveViaArcadeDrive()", e);
        }
    }


    public void driveOrClimb(GenericHID driverStick, GenericHID opsLeftStick, GenericHID opsRightStick)
    {
        if (inClimbMode)
        {
            if (useClimbSplitSticks)
            {
                driveViaTankDrive(opsLeftStick, opsRightStick);
            }
            else
            {
                climbViaSingleStick(opsLeftStick);
            }
        }
        else
        {
            driveViaArcadeDrive(driverStick);
        }
    }


    public void driveViaSwitchableDrive(GenericHID leftStick, GenericHID rightOrArcadeStick)
    {
        //updateJoyStickValues(leftStick, rightOrArcadeStick);


        if (inClimbMode || useTankDrive)
        {
            driveViaTankDrive(leftStick, rightOrArcadeStick);
        }
        else
        {
            driveViaArcadeDrive(rightOrArcadeStick);
        }
    }


    public void turnOnTankStickMode()
    {
        changeUseTankDriveStickMode(true);
    }


    public void turnOnArcadeMode()
    {
        changeUseTankDriveStickMode(false);
    }


    public boolean toggleUseTankDriveStickMode()
    {
        return changeUseTankDriveStickMode(!useTankDriveDriverSelection);
    }


    private boolean changeUseTankDriveStickMode(boolean useTankDriveSetting)
    {
        useTankDriveDriverSelection = useTankDriveSetting;
        useTankDrive = inClimbMode || useTankDriveDriverSelection;
        updateStickModeDisplay();
        return useTankDriveDriverSelection;
    }


    public void engageClimbMode()
    {
        LOG.debug("DriveTrainSubsystem.engageClimbMode() called");
        changeClimbMode(true);
    }


    public void disengageClimbMode()
    {
        LOG.debug("DriveTrainSubsystem.disengageClimbMode() called");
        changeClimbMode(false);
    }


    public boolean toggleClimbMode()
    {
        LOG.debug("DriveTrainSubsystem.disengageClimbMode() called");
        return changeClimbMode(!inClimbMode);
    }


    private boolean changeClimbMode(boolean inClimbMode)
    {
        LOG.info("DriveTrainSubsystem.changeClimbMode called. inClimbMode = " + inClimbMode);
        DriveTrainSubsystem.inClimbMode = inClimbMode;
        climbDisengageSolenoid.set(!inClimbMode);
        climbEngageSolenoid.set(inClimbMode);

        boolean invert = inClimbMode ? !invertRightSide : invertRightSide;
        robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, invert);
//        robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, inClimbMode);

        useTankDrive = inClimbMode || useTankDriveDriverSelection;
        updateClimbModeDisplay();
        return inClimbMode;
    }


    public void initDefaultCommand()
    {
        setDefaultCommand(new DriveCommand());
    }


    private void updateSensitivityModeDisplay()
    {
        SmartDashboard.putString("Drive Sens:", sensitivityModeStatus());
    }


    private void updateStickModeDisplay()
    {
        String mode = useTankDriveDriverSelection ? "Tank" : "Arcade";
        SmartDashboard.putString("Stick:", mode);
    }


    private void updateClimbModeDisplay()
    {
        String mode = inClimbMode ? "on" : "off";
        SmartDashboard.putString("Climb Mode", mode);
        SmartDashboard.putBoolean("In Climb Mode", inClimbMode);
    }


    private void updateJoyStickValues(GenericHID driverStick, GenericHID opsLeftStick, GenericHID opsRightStick)
    {
        if (RobotMap2013.IN_DEBUG_MODE)
        {
            if (driverStick != null)
            {
                SmartDashboard.putNumber("DriveX ", driverStick.getX());
                SmartDashboard.putNumber("DriveY ", driverStick.getY());
            }

            if (opsLeftStick != null)
            {
                SmartDashboard.putNumber("OpsLeftX ", opsLeftStick.getX());
                SmartDashboard.putNumber("OpsLeftY ", opsLeftStick.getY());
            }

            if (opsRightStick != null)
            {
                SmartDashboard.putNumber("OpsRightX ", opsRightStick.getX());
                SmartDashboard.putNumber("OpsRightY ", opsRightStick.getY());
            }
        }
    }


    private void initClimbSystem()
    {
        try
        {
            if (isClimbSystemEnabled)
            {
                LOG.debug("Initializing Climb System");
                climbEngageSolenoid = new Solenoid(RobotMap2013.Solenoids.GEAR_BOX_CLIMB_ENGAGE_ADDR);
                climbDisengageSolenoid = new Solenoid(RobotMap2013.Solenoids.GEAR_BOX_CLIMB_DISENGAGE_ADDR);

                leftClawLimitSwitch = new DigitalInput(RobotMap2013.DigitalIO.LEFT_CLAW_UP_POSITION_SWITCH_CHANNEL);
                rightClawLimitSwitch = new DigitalInput(RobotMap2013.DigitalIO.RIGHT_CLAW_UP_POSITION_SWITCH_CHANNEL);

                disengageClimbMode();
                updateClimbModeDisplay();
                LOG.debug("Climb System initialization completed successfully");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.initClimbSystem()", e);
        }

    }


    private void initRobotDrive()
    {
        try
        {
            if (isEnabled)
            {
                LOG.trace("initializing robotDrive");
                //frontLeftMotor, rearLeftMotor, frontRightMotor, rearRightMotor
//                robotDrive =
//                    new RobotDrive(RobotMap2013.DigitalIO.LEFT_FRONT_DRIVE_MOTOR,
//                                   RobotMap2013.DigitalIO.LEFT_REAR_DRIVE_MOTOR,
//                                   RobotMap2013.DigitalIO.RIGHT_FRONT_DRIVE_MOTOR,
//                                   RobotMap2013.DigitalIO.RIGHT_REAR_DRIVE_MOTOR);
//                robotDrive.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
//                robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
//                robotDrive.setInvertedMotor(RobotDrive.MotorType.kFrontRight, true);
//                robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, true);

                robotDrive = new RobotDrive(RobotMap2013.PWMChannels.LEFT_DRIVE_MOTOR, RobotMap2013.PWMChannels.RIGHT_DRIVE_MOTOR);
                robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, invertLeftSide);
                robotDrive.setInvertedMotor(RobotDrive.MotorType.kRearRight, invertRightSide);

            }
            else
            {
                LOG.info("DriveTrainSubsystem is disabled. Will not initialize it.");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in DriveTrainSubsystem.initRobotDrive()", e);
        }
    }

}
