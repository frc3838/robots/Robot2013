package frc3838.Y2013.subsystems;


import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.commands.shooting.RunShooterMotorsViaPotentiometerCommand;
import frc3838.Y2013.utils.LOG;



public class ShooterMotorsSubsystem extends Subsystem
{
    public static boolean isEnabled = true;
    public static boolean isInCalibrateMode = false;


    private static final ShooterMotorsSubsystem singleton = new ShooterMotorsSubsystem();

    public static final double minNonStopSpeed = 0.5;

    private static MotorControlMode MODE = MotorControlMode.Incrementally;

    private static final double INITIAL_LOWER_SPEED = 1;
    private static final double INITIAL_UPPER_SPEED = 1;
    private double lowerMotorSpeed = INITIAL_LOWER_SPEED;
    private double upperMotorSpeed = INITIAL_UPPER_SPEED;


    private MotorOps upperMotorOps;
    private MotorOps lowerMotorOps;


    /**
     * Gets the single instance of this subsystem. Use of this method replaces the use of a constructor such as <tt>new DriveTrainSubsystem()</tt> in order to use ensure only a
     * single instance of a Subsystem is created. This is known as a Singleton. For example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    public static ShooterMotorsSubsystem getInstance()
    {return singleton;}


    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern). Use the static {@link #getInstance()} method to obtain a reference to the subsystem. For
     * example, instead of doing this:
     * <pre>
     *     DriveTrainSubsystem subsystem = new DriveTrainSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     DriveTrainSubsystem subsystem = DriveTrainSubsystem.getInstance();
     * </pre>
     */
    private ShooterMotorsSubsystem()
    {
        //put initialization code in the init() method rather rather than here
        try
        {
            init();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.ShooterMotorsSubsystem()", e);
        }
    }


    public static MotorControlMode getMODE()
    {
        return MODE;
    }


    private void init()
    {
        if (isEnabled)
        {
            try
            {
                LOG.debug("Initializing ShooterMotorsSubsystem");
                initLowerShooterMotor();
                initUpperShooterMotor();
                LOG.debug("ShooterMotorsSubsystem initialization completed successfully");
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in ShooterMotorsSubsystem.init()", e);
            }

        }
        else
        {
            LOG.info("ShooterMotorsSubsystem is disabled and will not be initialized");
        }
    }


    private void initUpperShooterMotor()
    {
        if (isEnabled)
        {
            try
            {
                Victor upperShooterMotor = new Victor(RobotMap2013.PWMChannels.UPPER_SHOOTER_MOTOR);
                upperMotorOps = new MotorOps(upperShooterMotor, false, minNonStopSpeed);
                upperMotorOps.stop();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in ShooterMotorsSubsystem.initUpperShooterMotor()", e);
            }
        }
    }


    private void initLowerShooterMotor()
    {
        if (isEnabled)
        {
            try
            {
                Victor lowerShooterMotor = new Victor(RobotMap2013.PWMChannels.LOWER_SHOOTER_MOTOR);
                lowerMotorOps = new MotorOps(lowerShooterMotor, false, minNonStopSpeed);
                lowerMotorOps.stop();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in ShooterMotorsSubsystem.initUpperShooterMotor()", e);
            }
        }
    }


    public void setMotorSpeed(double speed)
    {
        try
        {
            if (isEnabled && inAPotentiometerMode())
            {
                lowerMotorSpeed = speed;
                upperMotorSpeed = speed;
                lowerMotorOps.setSpeed(lowerMotorSpeed);
                upperMotorOps.setSpeed(upperMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.setMotorSpeed()", e);
        }
    }


    public static boolean inAPotentiometerMode()
    {
        return (MODE.equals(MotorControlMode.Potentiometer) || MODE.equals(MotorControlMode.PotentiometerWithStartStop));
    }


    public static boolean inAStartStopMode()
    {
        return MODE.equals(MotorControlMode.Incrementally) || MODE.equals(MotorControlMode.PotentiometerWithStartStop);
    }


    public void startShooterMotors()
    {
        try
        {
            if (isEnabled && inAStartStopMode())
            {
                if (lowerMotorOps != null)
                {
                    lowerMotorOps.setSpeed(lowerMotorSpeed);
                }
                if (upperMotorOps != null)
                {
                    upperMotorOps.setSpeed(upperMotorSpeed);
                }
                displaySpeedSetting();
            }
            else
            {
                LOG.debug("Start command is not valid in Potentiometer mode. Call setSpeed");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.startShooterMotors()", e);
        }

    }


    public void stopShooterMotors()
    {
        try
        {
            if (isEnabled && inAStartStopMode())
            {
                if (lowerMotorOps != null)
                {
                    lowerMotorOps.stop();
                }
                if (upperMotorOps != null)
                {
                    upperMotorOps.stop();
                }
                displaySpeedSetting();
            }
            else
            {
                LOG.debug("Start command is not valid in Potentiometer mode. Call setSpeed");
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.stopShooterMotors()", e);
        }

    }


    public void displaySpeedSetting()
    {
        try
        {
            if (isEnabled)
            {
                if (lowerMotorSpeed == upperMotorSpeed)
                {
                    SmartDashboard.putString("Shooter %", lowerMotorOps.getSpeedPercentage());
                    SmartDashboard.putNumber("Shooter PB", lowerMotorSpeed * 100);
                    SmartDashboard.putNumber("Shooter ", lowerMotorSpeed);
                }
                else
                {
                    if (lowerMotorOps != null)
                    {
                        SmartDashboard.putString("LTM", lowerMotorOps.getSpeedPercentage());
                    }
                    else
                    {
                        SmartDashboard.putString("LTM", "isNull");
                    }
                    if (upperMotorOps != null)
                    {
                        SmartDashboard.putString("UTM", upperMotorOps.getSpeedPercentage());
                    }
                    else
                    {
                        SmartDashboard.putString("UTM", "isNull");
                    }
                }
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.displaySpeedSetting()", e);
        }

    }


    public void increaseLowerMotorSpeedOnePercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                lowerMotorSpeed = lowerMotorOps.constrainValue(lowerMotorSpeed + 0.01);
                lowerMotorOps.setSpeed(lowerMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.increaseLowerMotorSpeedOnePercent()", e);
        }
    }


    public void decreaseLowerMotorSpeedOnePercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                lowerMotorSpeed = lowerMotorOps.constrainValue(lowerMotorSpeed - 0.01);
                lowerMotorOps.setSpeed(lowerMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.decreaseLowerMotorSpeedOnePercent()", e);
        }
    }


    public void increaseLowerMotorSpeedTenPercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                lowerMotorSpeed = lowerMotorOps.constrainValue(lowerMotorSpeed + 0.1);
                lowerMotorOps.setSpeed(lowerMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.increaseLowerMotorSpeedTenPercent()", e);
        }
    }


    public void decreaseLowerMotorSpeedTenPercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                lowerMotorSpeed = lowerMotorOps.constrainValue(lowerMotorSpeed - 0.1);
                lowerMotorOps.setSpeed(lowerMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.decreaseLowerMotorSpeedTenPercent()", e);
        }
    }

    // ===============================================================


    public void increaseUpperMotorSpeedOnePercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                upperMotorSpeed = upperMotorOps.constrainValue(upperMotorSpeed + 0.01);
                upperMotorOps.setSpeed(upperMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.increaseUpperMotorSpeedOnePercent()", e);
        }
    }


    public void decreaseUpperMotorSpeedOnePercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                upperMotorSpeed = upperMotorOps.constrainValue(upperMotorSpeed - 0.01);
                upperMotorOps.setSpeed(upperMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.decreaseUpperMotorSpeedOnePercent()", e);
        }
    }


    public void increaseUpperMotorSpeedTenPercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                upperMotorSpeed = upperMotorOps.constrainValue(upperMotorSpeed + 0.1);
                upperMotorOps.setSpeed(upperMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.increaseUpperMotorSpeedTenPercent()", e);
        }
    }


    public void decreaseUpperMotorSpeedTenPercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                upperMotorSpeed = upperMotorOps.constrainValue(upperMotorSpeed - 0.1);
                upperMotorOps.setSpeed(upperMotorSpeed);
                displaySpeedSetting();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.decreaseUpperMotorSpeedTenPercent()", e);
        }
    }

    //===============================================================


    public void increaseBothMotorsSpeedOnePercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                increaseLowerMotorSpeedOnePercent();
                increaseUpperMotorSpeedOnePercent();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.increaseBothMotorsSpeedOnePercent()", e);
        }
    }


    public void decreaseBothMotorsSpeedOnePercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                decreaseLowerMotorSpeedOnePercent();
                decreaseUpperMotorSpeedOnePercent();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.decreaseBothMotorsSpeedOnePercent()", e);
        }
    }


    public void increaseBothMotorsSpeedTenPercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                increaseLowerMotorSpeedTenPercent();
                increaseUpperMotorSpeedTenPercent();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.increaseBothMotorsSpeedTenPercent()", e);
        }
    }


    public void decreaseBothMotorsSpeedTenPercent()
    {
        try
        {
            if (isEnabled && MODE.equals(MotorControlMode.Incrementally))
            {
                decreaseLowerMotorSpeedTenPercent();
                decreaseUpperMotorSpeedTenPercent();
            }
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.decreaseBothMotorsSpeedTenPercent()", e);
        }
    }


    public void changeMotorControlMode(MotorControlMode mode)
    {
        if (inAPotentiometerMode() && MotorControlMode.Incrementally.equals(mode))
        {
            setDefaultCommand(null);
        }
        else if (!inAPotentiometerMode() && !MotorControlMode.Incrementally.equals(mode))
        {
            setDefaultCommand(new RunShooterMotorsViaPotentiometerCommand());
        }
        MODE = mode;
    }


    public boolean areMotorsRunning()
    {
        try
        {
            return lowerMotorOps.isRunning() || upperMotorOps.isRunning();
        }
        catch (Exception e)
        {
            LOG.error("An exception occurred in ShooterMotorsSubsystem.areMotorsRunning()", e);
        }
        return true;
    }


    public void initDefaultCommand()
    {
        if (inAPotentiometerMode())
        {
            setDefaultCommand(new RunShooterMotorsViaPotentiometerCommand());
        }
    }
}
