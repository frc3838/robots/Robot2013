package frc3838.Y2013.commands.feedback;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc3838.Y2013.subsystems.AutonomousOptionSubsystem;
import frc3838.Y2013.utils.LOG;



public class ShowAutonomousOptionOnDashboardCommand extends frc3838.Y2013.commands.CommandBase
{

    public ShowAutonomousOptionOnDashboardCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(autonomousOptionSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (AutonomousOptionSubsystem.isEnabled)
        {
            try
            {
                String status = autonomousOptionSubsystem.getOptionAsDetailedString();
                LOG.info("Autonomous Option: " + status);
                SmartDashboard.putString("Autonomous Option", status);
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in ShowAutonomousOptionOnDashboardCommand.execute()", e);
            }

        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        //TODO: Set this properly
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
