package frc3838.Y2013.commands.shooting;


import frc3838.Y2013.commands.CommandBase;
import frc3838.Y2013.subsystems.FiringPinSubsystem;
import frc3838.Y2013.subsystems.ShooterMotorsSubsystem;
import frc3838.Y2013.utils.LOG;



public class ShootCommand extends CommandBase
{

    public ShootCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(firingPinSubsystem);
        requires(shooterAngleSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (FiringPinSubsystem.isEnabled &&
            ShooterMotorsSubsystem.isEnabled &&
            shooterMotorsSubsystem.areMotorsRunning())
        {
            LOG.debug("ShootCommand.execute() called and executing.");
            firingPinSubsystem.fire();
        }
        else
        {
            LOG.debug("ShootCommand.execute() called but guard conditions were not met. ");
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
