package frc3838.Y2013.commands.shooting;


import edu.wpi.first.wpilibj.Joystick;
import frc3838.Y2013.RobotMap2013;
import frc3838.Y2013.commands.CommandBase;
import frc3838.Y2013.subsystems.ShooterMotorsSubsystem;
import frc3838.Y2013.utils.LOG;
import frc3838.Y2013.utils.math.Math2;



public class RunShooterMotorsViaPotentiometerCommand extends CommandBase
{

    private static Joystick joystick = RobotMap2013.Joysticks.OPS_LEFT_JOYSTICK;

    private static final int percentageIncrements = 5;


    public RunShooterMotorsViaPotentiometerCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(shooterMotorsSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (!RobotMap2013.inAutonomousMode && ShooterMotorsSubsystem.isEnabled && ShooterMotorsSubsystem.inAPotentiometerMode())
        {
            try
            {
                double z = joystick.getZ();
                //z goes from -1 to 1... we want a value from 0 to 1 over that full range;
                double ranged = (z + 1) / 2;
                double speed = Math2.floorToXPercent(ranged, percentageIncrements);
                if (speed < ShooterMotorsSubsystem.minNonStopSpeed)
                {
                    speed = 0;
                }
                shooterMotorsSubsystem.setMotorSpeed(speed);
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in RunShooterMotorsViaPotentiometerCommand.execute()", e);
            }
        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return ShooterMotorsSubsystem.inAPotentiometerMode();
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
