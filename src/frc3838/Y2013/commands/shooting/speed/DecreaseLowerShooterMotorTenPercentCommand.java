package frc3838.Y2013.commands.shooting.speed;

import frc3838.Y2013.subsystems.ShooterMotorsSubsystem;
import frc3838.Y2013.utils.LOG;



public class DecreaseLowerShooterMotorTenPercentCommand extends frc3838.Y2013.commands.CommandBase
{

    public DecreaseLowerShooterMotorTenPercentCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(shooterMotorsSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (ShooterMotorsSubsystem.isEnabled)
        {
            try
            {
                shooterMotorsSubsystem.decreaseLowerMotorSpeedTenPercent();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in DecreaseLowerShooterMotorTenPercentCommand.execute()", e);
            }

        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        //TODO: Set this properly
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
