package frc3838.Y2013.commands.time;


import frc3838.Y2013.commands.CommandBase;



public class SleepMillisecondsCommand extends CommandBase
{
    private long startTime;
    private long delay;
    private boolean finished = false;


    public SleepMillisecondsCommand(long milliseconds)
    {
        delay = milliseconds;
    }


    public SleepMillisecondsCommand(int milliseconds)
    {
        delay = milliseconds;
    }


    // Called just before this Command runs the first time
    protected void initialize()
    {
        startTimer();
    }


    private void startTimer()
    {
        finished = false;
        startTime = System.currentTimeMillis();
    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        long diff = System.currentTimeMillis() - startTime;
        finished = diff >= delay;
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return finished;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
