package frc3838.Y2013.commands.picker;

import frc3838.Y2013.subsystems.PickerSubsystem;
import frc3838.Y2013.utils.LOG;



public class LowerPickerCommand extends frc3838.Y2013.commands.CommandBase
{

    public LowerPickerCommand()
    {
        // Use requires() here to declare subsystem dependencies
        // which must be declared as (static) fields in the CommandBase
        // eg. requires(driveTrain);
        //     requires(shooter);
        requires(pickerSubsystem);

    }


    // Called just before this Command runs the first time
    protected void initialize()
    {

    }


    // Called repeatedly when this Command is scheduled to run (until isFinished() returns true)
    protected void execute()
    {
        if (PickerSubsystem.isEnabled)
        {
            try
            {
                pickerSubsystem.lowerPicker();
            }
            catch (Exception e)
            {
                LOG.error("An exception occurred in LowerPickerCommand.execute()", e);
            }

        }
    }


    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return true;
    }


    // Called once after isFinished returns true
    // do any clean up or post command work here
    protected void end()
    {
    }


    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {

    }
}
